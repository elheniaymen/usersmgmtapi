# User Management Rest API

### User Requirements

What should be done:

A Springboot API that exposes two services:
- one that allows to register a user
- one that displays the details of a registered user

A user is defined by:
- a user name
- a birthdate
- a country of residence

A user has optional attributes:
- a phone number
- a gender

Only adult French residents are allowed to create an account!
Inputs must be validated and return proper error messages/http statuses.

Deliverables:
- Source code (Github link for example)
- Documentation (how to build from sources, how to use the API)
- Request samples (I.e. Postman collection)

Bonuses:
- Include an embedded database in your project to facilitate the installation and execution by someone else
- Use AOP to log inputs and outputs of each call as well as the processing time
- Feel free to enrich the model as you see fit

Make sure you pay special attention to code and test quality.

### Technical Architecture
![Technical Architecture](docs/archi/UsersManagementApi_TehchicalArchitecture.png)

### Run with docker
in order to run the project using docker-compose, execute the following shell commands:
```Bash
git clone https://gitlab.com/elheniaymen/usersmgmtapi.git
cd usersmgmtapi
./mvnw clean package
docker-compose down
docker network create my-network
docker-compose up -d --build
curl -X 'POST' \
  'http://localhost:8887/rest/api/users' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "username": "john1",
  "firstName": "John",
  "lastName": "Cleese",
  "email": "john.cleese@example.com",
  "phoneNumber": "+33613125565",
  "gender": "M",
  "birthDate": "2001-04-02",
  "country": "FR"
}'
curl -X 'GET' \
  'http://localhost:8887/rest/api/users/<user-id>' \
  -H 'accept: application/json'
```

 You can freely use the version already deployed in this URL http://62.171.159.134:8887/rest/api/users via the swagger-ui interface
 using this url http://62.171.159.134:8887/swagger-ui/index.html
 
 You can use Postman by importing the file UserManagementApi.postman_collection.json
  
  
### API Documentation
[Documentation Link](http://62.171.159.134:8887/swagger-ui/index.html)
