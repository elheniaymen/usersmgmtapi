package net.atos.testoffer.usersmanagementapi.service;

import org.springframework.stereotype.Service;

import net.atos.testoffer.usersmanagementapi.dto.UserDTO;
import net.atos.testoffer.usersmanagementapi.exception.UserAlreadyExistsException;

@Service
public interface UsersService {

	UserDTO registerUser(UserDTO user) throws UserAlreadyExistsException;
	
	UserDTO getUserDetails(String id);
}
