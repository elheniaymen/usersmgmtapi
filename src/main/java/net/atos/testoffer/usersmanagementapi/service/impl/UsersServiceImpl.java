package net.atos.testoffer.usersmanagementapi.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.atos.testoffer.usersmanagementapi.constantes.APIConstantes;
import net.atos.testoffer.usersmanagementapi.dto.UserDTO;
import net.atos.testoffer.usersmanagementapi.exception.UserAlreadyExistsException;
import net.atos.testoffer.usersmanagementapi.model.User;
import net.atos.testoffer.usersmanagementapi.repository.UserRepository;
import net.atos.testoffer.usersmanagementapi.service.UsersService;

@Service
public class UsersServiceImpl implements UsersService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
    private ModelMapper modelMapper;
	@Override
	public UserDTO registerUser(UserDTO user) throws UserAlreadyExistsException{
		List<User> userList = userRepository.findByUsername(user.getUsername());
		if(!Objects.isNull(userList) && !userList.isEmpty()) {
			throw new UserAlreadyExistsException(APIConstantes.MSG_A_USER_ALREADY_EXISTS);
		}
		return convertToDTO(userRepository.save(convertToEntity(user)));
	}
	
	@Override
	public UserDTO getUserDetails(String id) {
		Optional<User> optionalUser = userRepository.findById(id);
		if(optionalUser.isEmpty()) {
			return null;
		}
		
		return convertToDTO(optionalUser.get());
	}
	
	private UserDTO convertToDTO(User user) {
		if(user == null) {
			return null;
		}
		return modelMapper.map(user, UserDTO.class);
	}

	private User convertToEntity(UserDTO userDTO) {
		if(userDTO == null) {
			return null;
		}
		return modelMapper.map(userDTO, User.class);
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}
	
}
