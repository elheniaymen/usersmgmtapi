package net.atos.testoffer.usersmanagementapi.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.v3.oas.annotations.media.Schema;
import net.atos.testoffer.usersmanagementapi.validator.annotation.IsAdult;
import net.atos.testoffer.usersmanagementapi.validator.annotation.IsLocalResident;

/**
 * DTO to represent a user
 * @author elheni
 *
 */

public class UserDTO {
	
	/**
	 * Technical Id of a user
	 */
    @Schema(description = "Technical generated unique Id", example = "ABCDDESSSS")
	private String id;
	
	/**
	 * The user username
	 */
	@NotBlank(message = "Username is mandatory")
	@Schema(description = "Username that identify the user ", example = "username")
	private String username;
	
	/**
	 * The user gender
	 */
	@Schema(description = "The user gender", example ="F", allowableValues = {"F", "M", "O"})
	@Pattern(regexp = "[FMO]", message = "The gender must be F for Female, M for Male and O for Other")
	private String gender;
	
	/**
	 * The user First Name
	 */
	@Schema(description = "The user first name", example ="Margaret")
	private String firstName;
	
	/**
	 * The user Last Name
	 */
	@Schema(description = "The user last name", example = "Thatcher")
	private String lastName;
	
	
	/**
	 * The user date of birth accepted format is dd/MM/yyyy
	 */
	@NotNull(message = "Birth Date is mandatory")
	@IsAdult
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Schema(description = "The user date of birth", example = "1925-10-13")
	private LocalDate birthDate;
	
	/**
	 * The user Email
	 */
	@Pattern(regexp = "^.*@.*$", message = "Email is not conform to standard format")
	@Schema(description = "The user email", example = "margaret.thatcher@downingstreet.uk")
	private String email;
	
	/**
	 * The user Country 2 letters Code 
	 */
	@NotBlank(message = "Country code is mandatory")
	@Pattern(regexp = "^[A-Z]{2}$", message = "Country code must contain only 2 upper case letters")
	@IsLocalResident
	@Schema(description = "The user country of residence", example = "FR")
	private String country;
	
	/**
	 * The user phone number 
	 */
	@Pattern(regexp = "^\\+\\d{5,15}$", message = "Phone number is not valid")
	@Schema(description = "The user phone number", example = "+33613169568")
	private String phoneNumber;
    

    public UserDTO(){
    	super();
    }

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
    }
	
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", username=" + username + ", gender=" + gender + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", birthDate=" + birthDate + ", email=" + email + ", country=" + country
				+ ", phoneNumber=" + phoneNumber + "]";
	}

	public static UserDTO build() {
		return new UserDTO();
	}
	
	public UserDTO username(String username) {
		this.setUsername(username);
		return this;
	}
	public UserDTO firstName(String firstName) {
		this.setFirstName(firstName);
		return this;
	}
	public UserDTO lastName(String lastName) {
		this.setLastName(lastName);
		return this;
	}

	public UserDTO birthDate(LocalDate birthDate) {
		this.setBirthDate(birthDate);
		return this;
	}
	public UserDTO email(String email) {
		this.setEmail(email);
		return this;
	}
	public UserDTO phoneNumber(String phoneNumber) {
		this.setPhoneNumber(phoneNumber);
		return this;
	}
	
	public UserDTO country(String country) {
		this.setCountry(country);
		return this;
	}
	
	public UserDTO id(String id) {
		this.setId(id);
		return this;
	}
}
