package net.atos.testoffer.usersmanagementapi.dto;

/**
 * Java DTO used to return error message in the rest response
 * 
 * @author elheni
 *
 */
public class UsersRestApiErrorDTO {

	/**
	 * The error message
	 */
	private String errorMessage;
	public UsersRestApiErrorDTO() {
		super();
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public static UsersRestApiErrorDTO build() {
		return new UsersRestApiErrorDTO();
	}
	
	public UsersRestApiErrorDTO errorMessage(String errorMessage) {
		this.setErrorMessage(errorMessage);
		return this;
	}

	@Override
	public String toString() {
		return "UsersRestApiError [errorMessage=" + errorMessage + "]";
	}
}
