package net.atos.testoffer.usersmanagementapi.rest;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.atos.testoffer.usersmanagementapi.dto.UserDTO;
import net.atos.testoffer.usersmanagementapi.dto.UsersRestApiErrorDTO;
import net.atos.testoffer.usersmanagementapi.exception.UserAlreadyExistsException;
import net.atos.testoffer.usersmanagementapi.service.UsersService;

/**
 * Implementation of the rest Services
 * @author elheni
 *
 */
@RestController
@RequestMapping(path = "/rest/api/", produces = "application/json")
@Tag(name = "users", description = "The Users management API")
public class UserRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

	@Autowired
	private UsersService usersService;

	
	/**
	 * Get Service that returns a @User given a technical Id
	 * @param id Technical Identifier of the user
	 * @return @ResponseEntity<@Object>
	 */
	@Operation(summary = "Display User details", description = "Display User details", tags = {"users"})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "The user was registered successfully", content = @Content(schema = @Schema(implementation = UserDTO.class))),
			@ApiResponse(responseCode = "404", description = "The user was not found")
	})
	@GetMapping(value = "/users/{id}", produces =  {"application/json"} )
	public ResponseEntity<Object> getUserDetails(@PathVariable @Parameter()String id){
		UserDTO userDto = usersService.getUserDetails(id);
		if(userDto == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("GET /user/{} : RESULT user with id {} returned",id, id);
		}
		
		return ResponseEntity.ok(userDto);
	}
	
	/**
	 * Post Service that validate and create a user in the database
	 * @param user @User submitted
	 * @return ResponseEntity<Object>
	 */
	@Operation(summary = "Register a user", description = "Register a user in the database", tags = {"users"})
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "The user was registered successfully", content = @Content(schema = @Schema(implementation = UserDTO.class))),
			@ApiResponse(responseCode = "400", description = "The input fields were not valid", content = @Content(schema = @Schema(implementation = UsersRestApiErrorDTO.class))),
	})
	@PostMapping(value = "/users", produces =  {"application/json"}, consumes = {"application/json"})
	public ResponseEntity<Object> registerUser(@Valid @RequestBody UserDTO userDto){
		try {
			UserDTO savedUserDTO = usersService.registerUser(userDto);
			return ResponseEntity.ok(savedUserDTO);
		} catch (UserAlreadyExistsException e) {
			LOGGER.error(e.getMessage(), e);
			return ResponseEntity.badRequest().body(UsersRestApiErrorDTO.build().errorMessage(e.getMessage()));
		}
	}

	/**
	 * Return a 400 Http code and errors when sumitted user failed the validation process
	 * @param ex @MethodArgumentNotValidException 
	 * @return @Map<@String, @String>
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(
	  MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach(error -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return errors;
	}
	
	/**
	 * Return a 500 Http code and errors when internal server error
	 * @param ex @Exception
	 * @return @String
	 */
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public String handleServerErrors(
	  Exception ex) {
	    return ex.getMessage();
	}
}
