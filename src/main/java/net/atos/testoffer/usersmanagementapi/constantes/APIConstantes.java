package net.atos.testoffer.usersmanagementapi.constantes;

public class APIConstantes {
	
	private APIConstantes() {
		
	}
	
	public static final String MSG_A_USER_ALREADY_EXISTS = "A user already exists with the same username";
}
