package net.atos.testoffer.usersmanagementapi.exception;

public class UserAlreadyExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2894981969083120418L;

	public UserAlreadyExistsException(String message) {
		super(message);
	}

	
}
