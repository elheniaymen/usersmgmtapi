package net.atos.testoffer.usersmanagementapi.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * DTO to represent a user
 * @author elheni
 *
 */
@Document("users")
public class User {
	/**
	 * Technical Id of a user
	 */
	@Id
    private String id;
	
	/**
	 * The user username
	 */
	private String username;
	
	/**
	 * The user Gender
	 */
	private String gender;
	
	/**
	 * The user First Name
	 */
	private String firstName = "";
	
	/**
	 * The user Last Name
	 */
	private String lastName = "";
	
	/**
	 * The user password
	 */
	private String password;
	
	/**
	 * The user date of birth
	 */
	private LocalDate birthDate;
	
	/**
	 * The user Email
	 */
	private String email;
	
	/**
	 * The user Country 2 letters Code 
	 */
	private String country;
	
	/**
	 * The user phone number 
	 */
	private String phoneNumber = "";
    

    public User(){
    	super();
    }

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
    }
	
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", gender=" + gender + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", password=" + password + ", birthDate=" + birthDate + ", email=" + email
				+ ", country=" + country + ", phoneNumber=" + phoneNumber + "]";
	}
}
