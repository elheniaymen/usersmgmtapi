package net.atos.testoffer.usersmanagementapi.validator.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import net.atos.testoffer.usersmanagementapi.validator.IsLocalResidentValidator;

/**
 * Java anotation used to the a user country if it's local or not
 * @author elheni
 *
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IsLocalResidentValidator.class)
public @interface IsLocalResident {
    String message() default "The user is not a local resident";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    
	/**
	 * Defines several {@link IsLocalResident} annotations on the same element.
	 *
	 * @see IsLocalResident
	 */
	@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
	@Retention(RUNTIME)
	@Documented
	@interface List {

		IsLocalResident[] value();
	}
}
