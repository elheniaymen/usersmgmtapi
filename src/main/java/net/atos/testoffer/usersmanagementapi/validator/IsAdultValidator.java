package net.atos.testoffer.usersmanagementapi.validator;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.atos.testoffer.usersmanagementapi.validator.annotation.IsAdult;

/**
 * A validator that check the age of a user in order to verify if it's adult or not
 * it's linked to the annotation @IsAdult
 * 
 * @author elheni
 *
 */
@Component
public class IsAdultValidator implements ConstraintValidator<IsAdult, LocalDate> {

	@Value("${usermanangementapi.adultAge}")
	private Integer age;
	@Override
	public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
		if(Objects.isNull(value)) {
			return true;
		}
		Integer userAge = calculateAgeInYear(value);
		return userAge >= age;
	}
	
	private Integer calculateAgeInYear(LocalDate dateOfBirth) {
		LocalDate actualDate = LocalDate.now();
        return Period.between(dateOfBirth, actualDate).getYears();
	}
	
	public Integer getAge() {
		return age;
	}
	
	public void setAge(Integer age) {
		this.age = age;
	}

}
