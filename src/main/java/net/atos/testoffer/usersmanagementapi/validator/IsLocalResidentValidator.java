package net.atos.testoffer.usersmanagementapi.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import net.atos.testoffer.usersmanagementapi.validator.annotation.IsLocalResident;

/**
 * 
 * A validator linked to the annotation @IsResidentOf
 * @author elheni
 *
 */
@Component
public class IsLocalResidentValidator implements ConstraintValidator<IsLocalResident, String> {

	@Value("${usermanangementapi.localCountryCode}")
	private String countryCode;
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(value == null) {
			return true;
		}
		
		if(StringUtils.hasLength(value)) {
			return countryCode.contentEquals(value.toUpperCase());
		}
		
		return false;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
