package net.atos.testoffer.usersmanagementapi;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring Boot Runner Application
 * @author A622187
 *
 */
@SpringBootApplication
@Configuration
public class UsersRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsersRestApiApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
}
