package net.atos.testoffer.usersmanagementapi.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.modelmapper.ModelMapper;

import net.atos.testoffer.usersmanagementapi.dto.UserDTO;
import net.atos.testoffer.usersmanagementapi.exception.UserAlreadyExistsException;
import net.atos.testoffer.usersmanagementapi.model.User;
import net.atos.testoffer.usersmanagementapi.repository.UserRepository;

class UsersServiceImplTest {

	private UsersServiceImpl userServiceImpl;
	

	@BeforeEach
	void setUp() throws Exception {
		userServiceImpl = new UsersServiceImpl();
		userServiceImpl.setModelMapper(new ModelMapper());
	}


	@Test
	void test_GivenUserId_WhenUserExist_ThanReturnUserDetails() {
		UserRepository userRepository = mock(UserRepository.class);
		User user = new User();
		user.setUsername("username");
		user.setBirthDate(LocalDate.of(1983, 4, 2));
		user.setEmail("test@test.fr");
		user.setCountry(Locale.FRANCE.getCountry());
		user.setId("1");
		when(userRepository.findById("1")).thenReturn(Optional.of(user));
		userServiceImpl.setUserRepository(userRepository);
		
		UserDTO returnedUser = userServiceImpl.getUserDetails("1");
		assertNotNull(returnedUser);
		assertThat(returnedUser.getUsername()).isEqualTo("username");
		assertThat(returnedUser.getBirthDate()).isEqualTo(LocalDate.of(1983, 4, 2));
		assertThat(returnedUser.getEmail()).isEqualTo("test@test.fr");
		assertThat(returnedUser.getCountry()).isEqualTo(Locale.FRANCE.getCountry());
		
		
	}
	
	void test_GivenUserId_WhenUserNotExist_ThanReturnNull() {
		UserRepository userRepository = mock(UserRepository.class);
		when(userRepository.findById("2")).thenReturn(Optional.of(null));
		userServiceImpl.setUserRepository(userRepository);
		
		UserDTO returnedUser = userServiceImpl.getUserDetails("2");
		assertNull(returnedUser);
		
	}
	
	@Test
	void test_GivenUser_WhenUsernameExist_ThanThrowUserAlreadyExistException() {
		UserRepository userRepository = mock(UserRepository.class);
		User user = new User();
		user.setUsername("username");
		user.setBirthDate(LocalDate.of(1983, 4, 2));
		user.setEmail("test@test.fr");
		user.setCountry(Locale.FRANCE.getCountry());
		user.setId("1");
		when(userRepository.findByUsername("username")).thenReturn(Collections.singletonList(user));
		userServiceImpl.setUserRepository(userRepository);
		UserDTO userToRegister = new UserDTO();
		userToRegister.setUsername("username");
		userToRegister.setBirthDate(LocalDate.of(1983, 4, 2));
		userToRegister.setEmail("test@test.fr");
		userToRegister.setCountry(Locale.FRANCE.getCountry());
		
		assertThrows(UserAlreadyExistsException.class, new Executable() {
			
			@Override
			public void execute() throws Throwable {
				userServiceImpl.registerUser(userToRegister);
			}
		});
	}
	
	@Test
	void test_GivenUser_WhenUsernameNotExist_ThanCreateUser() {
		UserRepository userRepository = mock(UserRepository.class);
		User user = new User();
		user.setUsername("username");
		user.setBirthDate(LocalDate.of(1983, 4, 2));
		user.setEmail("test@test.fr");
		user.setCountry(Locale.FRANCE.getCountry());
		user.setId("1");
		when(userRepository.save(any(User.class))).thenReturn(user);
		userServiceImpl.setUserRepository(userRepository);
		UserDTO userToRegister = new UserDTO();
		userToRegister.setUsername("username");
		userToRegister.setBirthDate(LocalDate.of(1983, 4, 2));
		userToRegister.setEmail("test@test.fr");
		userToRegister.setCountry(Locale.FRANCE.getCountry());
		try {
			UserDTO returnedUser = userServiceImpl.registerUser(userToRegister);
			assertNotNull(returnedUser);
			assertThat(returnedUser.getId()).isEqualTo("1");
			assertThat(returnedUser.getBirthDate()).isEqualTo(LocalDate.of(1983, 4, 2));
			
		} catch (UserAlreadyExistsException e) {
			fail(e.getMessage());
		}
	}

}
