package net.atos.testoffer.usersmanagementapi.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Locale;

import javax.validation.ConstraintValidatorContext;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

class IsLocalResidentValidatorTest {
	@MockBean
	private ConstraintValidatorContext constraintValidatorContext;
	
	@Test
	void test_GivenCountryCode_WhenEqualToFR_ThanExpectTrue() {
		IsLocalResidentValidator validator = new IsLocalResidentValidator();
		validator.setCountryCode(Locale.FRANCE.getCountry());
		boolean isValid = validator.isValid(
				Locale.FRANCE.getCountry(),
				constraintValidatorContext);
		assertTrue(isValid);
	}
	
	@Test
	void test_GivenCountryCode_WhenNotEqualToFR_ThanExpectFalse() {
		IsLocalResidentValidator validator = new IsLocalResidentValidator();
		validator.setCountryCode(Locale.FRANCE.getCountry());
		boolean isValid = validator.isValid(
				Locale.GERMAN.getCountry(),
				constraintValidatorContext);
		assertFalse(isValid);
	}

}
