/**
 * 
 */
package net.atos.testoffer.usersmanagementapi.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import javax.validation.ConstraintValidatorContext;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * Validation tests
 * 
 * @author elheni
 *
 */
class IsAdultValidatorTest {
	@MockBean
	private ConstraintValidatorContext constraintValidatorContext;

	@Test
	void test_GivenBirthDate_WhenAgeLessThan18_ReturnFalse() {
		IsAdultValidator validator = new IsAdultValidator();
		validator.setAge(18);
		LocalDate calculatedBirthDate = LocalDate.now().minusYears(17);
		boolean isValid = validator.isValid(
				calculatedBirthDate,
				constraintValidatorContext);
		assertFalse(isValid);
	}

	@Test
	void test_GivenBirthDate_WhenAgeGreaterThan18_ReturnTrue() {
		IsAdultValidator validator = new IsAdultValidator();
		validator.setAge(18);
		LocalDate calculatedBirthDate = LocalDate.now().minusYears(19);
		boolean isValid = validator.isValid(
				calculatedBirthDate,
				constraintValidatorContext);
		assertTrue(isValid);
	}

	@Test
	void test_GivenBirthDate_WhenAgeEquals18_ReturnTrue() {
		IsAdultValidator validator = new IsAdultValidator();
		validator.setAge(18);
		LocalDate calculatedBirthDate = LocalDate.now().minusYears(18);
		boolean isValid = validator.isValid(
				calculatedBirthDate,
				constraintValidatorContext);
		assertTrue(isValid);
	}

}
