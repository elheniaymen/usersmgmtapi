package net.atos.testoffer.usersmanagementapi;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import net.atos.testoffer.usersmanagementapi.constantes.APIConstantes;
import net.atos.testoffer.usersmanagementapi.dto.UserDTO;
import net.atos.testoffer.usersmanagementapi.dto.UsersRestApiErrorDTO;
import net.atos.testoffer.usersmanagementapi.repository.UserRepository;

/**
 * Integration Tests
 * @author elheni
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class UsersRestApiIntegrationTest {

	private static final String STR_HTTP_LOCALHOST = "http://localhost:";

	private static final String STR_REST_API_USERS = "/rest/api/users";

	@LocalServerPort
	private int port;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@AfterEach
	void tearDown() throws Exception {
		userRepository.deleteAll();
	}
	
	@Test
	void test_GivenUser_WhenRegisterMandatoryFieldIsEmpty_ThanBadRequest() {
		UserDTO user = new UserDTO();
		user.setUsername("");
		user.setBirthDate(LocalDate.of(1983, 4, 2));
		user.setEmail("test@test.fr");
		ResponseEntity<String> result = this.restTemplate.postForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS, user,
				String.class);
		assertThat(result).isNotNull();
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);		
	}
	
	@Test
	void test_GivenUser_WhenRegisterCountryCodeIsNotFR_ThanBadRequest() {
		UserDTO user = new UserDTO();
		user.setUsername("test");
		user.setBirthDate(LocalDate.of(1983, 4, 2));
		user.setEmail("test@test.fr");
		user.setCountry("EN");
		
		ResponseEntity<Object> result = this.restTemplate.postForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS, user,
				Object.class);
		assertThat(result).isNotNull();
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(result.getBody()).isInstanceOf(Map.class);
		Map<String, String> errors = (Map<String, String>) result.getBody();
		assertThat(errors).isNotNull().containsOnlyKeys("country");
		assertThat(errors.values()).containsOnly("The user is not a local resident");
		
	}
	
	@Test
	void test_GivenUser_WhenRegisterNotAdult_ThanBadRequest() {
		UserDTO user = new UserDTO();
		user.setUsername("test");
		LocalDate calculatedBirthDate = LocalDate.now().minusYears(17);
		user.setBirthDate(calculatedBirthDate);
		user.setEmail("test@test.fr");
		user.setCountry("FR");
		ResponseEntity<Object> result = this.restTemplate.postForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS, user,
				Object.class);
		assertThat(result).isNotNull();
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(result.getBody()).isInstanceOf(Map.class);
		Map<String, String> errors = (Map<String, String>) result.getBody();
		assertThat(errors).isNotNull().containsOnlyKeys("birthDate");
		assertThat(errors.values()).containsOnly("User must be adult");
		
	}
	
	@Test
	void test_GivenUser_WhenRegisterExistingUsername_ThanBadRequest() {
		
		String username = "unittest1";
		UserDTO user = UserDTO.build().username(username).firstName("Unit")
				.lastName("Test").birthDate(LocalDate.of(1980, 4, 2))
				.email("junit.test@java.fr").phoneNumber("+33611111111")
				.country("FR");
		{
			ResponseEntity<UserDTO> result = this.restTemplate.postForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS,
					user, UserDTO.class);
			assertThat(result).isNotNull();
			assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		}
		{
			ResponseEntity<UsersRestApiErrorDTO> result = this.restTemplate
					.postForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS, user, UsersRestApiErrorDTO.class);
			assertThat(result).isNotNull();
			assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
			UsersRestApiErrorDTO apiError = result.getBody();
			assertThat(apiError).isNotNull().hasFieldOrPropertyWithValue("errorMessage", APIConstantes.MSG_A_USER_ALREADY_EXISTS);
		}
	}
	
	@Test
	void test_GivenUser_WhenRegister_ThanCreateUser() {
		UserDTO user = new UserDTO();
		user.setUsername("test");
		user.setBirthDate(LocalDate.of(1983, 4, 2));
		user.setEmail("test@test.fr");
		user.setCountry("FR");
		
		
		ResponseEntity<UserDTO> result = this.restTemplate.postForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS, user,
				UserDTO.class);
		assertThat(result).isNotNull();
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		UserDTO newUser = result.getBody();
		assertThat(newUser).isNotNull();
		assertThat(newUser.getId()).isNotNull();
	}
	
	@Test
	void test_GivenUser_WhenFindByUserId_ThanReturnUser() {
		String userId = null;
		{
			UserDTO user = UserDTO.build().username("unittest1").firstName("Unit")
					.lastName("Test").birthDate(LocalDate.of(1983, 4, 2))
					.email("junit.test@java.fr").phoneNumber("+33611111111")
					.country("FR");
			ResponseEntity<UserDTO> result = this.restTemplate.postForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS, user,
					UserDTO.class);		
			assertThat(result).isNotNull();
			assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
			UserDTO createdUser = result.getBody();
			assertThat(createdUser).isNotNull();
			userId = createdUser.getId();
		}
		{
			ResponseEntity<UserDTO> result = this.restTemplate.getForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS + "/"+String.valueOf(userId), 
					UserDTO.class);	
			assertThat(result).isNotNull();
			assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
			UserDTO newUser = result.getBody();
			assertThat(newUser).isNotNull();
			assertThat(newUser.getId()).isNotNull().isEqualTo(userId);
			assertThat(newUser.getUsername()).isNotNull().isEqualTo("unittest1");
			assertThat(newUser.getFirstName()).isNotNull().isEqualTo("Unit");
			assertThat(newUser.getLastName()).isNotNull().isEqualTo("Test");
			assertThat(newUser.getEmail()).isNotNull().isEqualTo("junit.test@java.fr");
			assertThat(newUser.getPhoneNumber()).isNotNull().isEqualTo("+33611111111");
			assertThat(newUser.getCountry()).isNotNull().isEqualTo(Locale.FRANCE.getCountry());
			assertThat(newUser.getBirthDate()).isNotNull().isEqualTo(LocalDate.of(1983, 4, 2));
		}
	}
	
	@Test
	void test_GivenUser_WhenFindByUserId_ThanNoUserFound() {
		{
			ResponseEntity<UserDTO> result = this.restTemplate.getForEntity(STR_HTTP_LOCALHOST + port + STR_REST_API_USERS + "/2", 
					UserDTO.class);	
			assertThat(result).isNotNull();
			assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		}
	}
	
}
